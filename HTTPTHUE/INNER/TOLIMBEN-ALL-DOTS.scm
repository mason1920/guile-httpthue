(define-module (HTTPTHUE INNER TOLIMBEN-ALL-DOTS))

(define TOLIMBEN-ALL-OEN-DOTS (lambda (LIMBS) (let ((HAF-HAFT (delete "/." LIMBS))) (cond
  ((or
     (equal? HAF-HAFT (list))
     (equal? HAF-HAFT (list "."))
     (equal? HAF-HAFT (list "." "/"))
  ) (list))
  ((equal? (car HAF-HAFT) ".") (cons (substring (cadr HAF-HAFT) 1) (cddr HAF-HAFT)))
  (#t HAF-HAFT)
))))

(define TOLIMBEN-ALL-TWA-DOTS (lambda (LIMBS) (letrec ((INNER (lambda (IN OUT) (cond
  ((or
     (equal? IN (list))
     (equal? IN (list ".."))
     (equal? IN (list ".." "/"))
  ) OUT)
  ((equal? (car IN) "..") (INNER (cons (substring (cadr IN) 1) (cddr IN)) OUT))
  ((equal? (car IN) "/..") (cond
    ((equal? (length OUT) 0) (INNER (cdr IN) OUT))
    ((and
      (equal? (length OUT) 1)
      (not (equal? (string-ref (car OUT) 0) #\/))
    ) (INNER (cons (substring (cadr IN) 1) (cddr IN)) (list)))
    (#t (INNER (cdr IN) (list-head OUT (- (length OUT) 1))))
  ))
  (#t (INNER (cdr IN) (append OUT (list (car IN)))))
)))) (INNER LIMBS (list)))))

(define LIMBEN-PATH (lambda (PATH) (cond
  ((equal? PATH "") (list))
  (#t (let ((ENDBIRDNESS (let ((MAEENDBIRDNESS (string-index (substring PATH 1) (lambda (STAFF) (equal? STAFF #\/))))) (cond
      (MAEENDBIRDNESS (+ MAEENDBIRDNESS 1))
      (#t (string-length PATH))
  )))) (cons (substring PATH 0 ENDBIRDNESS) (LIMBEN-PATH (substring PATH ENDBIRDNESS)))))
)))

; HTTPS://DATATRACKER.IETF.ORG/doc/html/rfc3986/#section-5.2.4
(define-public TOLIMBEN-ALL-DOTS (lambda (PATH) 
	(string-join
		(TOLIMBEN-ALL-TWA-DOTS (TOLIMBEN-ALL-OEN-DOTS
			(LIMBEN-PATH PATH)
		))
		""
	)
))
