(define-module (HTTPTHUE INNER ANKWEETHS))

(use-modules
	(ice-9 textual-ports)
	(sph libmagic)
	(web response)
  (web http)
)

(define MISFAER (lambda (RIEM) (lambda () (values
	(build-response #:code RIEM)
	""
))))

; SPEED

(define-public OEKAE (lambda (HOELDINGWAE) (values
	(build-response
		#:code 200
		#:headers (list
			(cons 'content-type
				(append
					(parse-header 'content-type (car (file->mime-types HOELDINGWAE)))
					(list (cons 'charset "UTF-8"))
				)
			)
		)
	)
  (get-string-all (open-file HOELDINGWAE "r" #:encoding "UTF-8"))
)))

; WAE

(define-public EVURLEE-FAEDED (lambda (URIWAE) (values
	(build-response
		#:code 308
		#:headers (list (cons 'location (parse-header 'location URIWAE)))
	)
	""
)))

; NOETOOER MISFAER

(define-public UNFOUND (MISFAER 404))

; THUER MISFAER

(define-public UNFULCUMD (MISFAER 501))
