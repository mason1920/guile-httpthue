(define-module (HTTPTHUE))

(use-modules
  (HTTPTHUE INNER ANKWEETHS)
  (HTTPTHUE INNER TOLIMBEN-ALL-DOTS)
  (ice-9 regex)
  (web request)
  (web server)
  (web uri)
)

(define URIWAE-ROOTED? (lambda (URIWAE) (string-prefix? "/" URIWAE)))
(define URIWAE-HOELDERWAE? (lambda (URIWAE) (equal? (string-ref URIWAE (- (string-length URIWAE) 1)) #\/)))
(define URIWAE->HOELDINGWAE (lambda (URIWAE)
  (regexp-substitute/global #f "/" URIWAE 'pre file-name-separator-string 'post)
))

(define HOELDINGWAE-FOERGOE-ENDSHED (lambda (HOELDINGWAE)
  (define FINGER-MAEENDSHED (- (string-length HOELDINGWAE) (string-length file-name-separator-string)))
  (cond
    ((equal? (substring HOELDINGWAE FINGER-MAEENDSHED) file-name-separator-string) (substring HOELDINGWAE 0 FINGER-MAEENDSHED))
    (#t HOELDINGWAE)
  )
))
(define HOELDINGWAE-HOELDER? (lambda (HOELDINGWAE) (equal? (stat:type (stat HOELDINGWAE)) 'directory)))
(define HOELDINGWAE-REEDENDLEE? (lambda (HOELDINGWAE) (access? HOELDINGWAE R_OK)))

; WEN LINKING FRUM 127.0.0.1, URI HOST WUS #f AND TOLD OF WEENING A PATH NOT LEEDING WITH SUM EMPTEE LIMBS.
; SOO MEE COES FOR EMPTEE LIMBS TOO BEE UNFOUND
(define FIEND (lambda (HOELDERWAE URIWAE)
  (define HOELDERWAE-ROOTED (canonicalize-path HOELDERWAE))
  (define URIWAE-RIETED (TOLIMBEN-ALL-DOTS URIWAE))
  (define LOED (HOELDINGWAE-FOERGOE-ENDSHED (string-append HOELDERWAE-ROOTED (URIWAE->HOELDINGWAE URIWAE-RIETED))))
  (define TEL (string-append LOED file-name-separator-string "index.html"))
  (cond
    ((not (URIWAE-ROOTED? URIWAE-RIETED)) (UNFULCUMD))
    ((not (HOELDINGWAE-REEDENDLEE? LOED)) (UNFOUND))
    ((URIWAE-HOELDERWAE? URIWAE-RIETED) (cond
      (
        (not (HOELDINGWAE-HOELDER? LOED))
        (EVURLEE-FAEDED (substring URIWAE-RIETED 0 (- (string-length URIWAE-RIETED) (string-length "/"))))
      )
      (#t (cond
        ((not (HOELDINGWAE-REEDENDLEE? TEL)) (UNFOUND))
        ((not (equal? URIWAE URIWAE-RIETED)) (EVURLEE-FAEDED URIWAE-RIETED))
        (#t (OEKAE TEL))
      ))
    ))
    (#t (cond
      ((HOELDINGWAE-HOELDER? LOED) (cond
        ((HOELDINGWAE-REEDENDLEE? TEL) (EVURLEE-FAEDED (string-append URIWAE-RIETED "/")))
        (#t (UNFOUND))
      ))
      (#t (cond
        ((not (equal? URIWAE URIWAE-RIETED)) (EVURLEE-FAEDED URIWAE-RIETED))
        (#t (OEKAE LOED))
      ))
    ))
  )
))

(define-public HTTPTHUE (lambda* (#:optional (HOELDERWAE (getcwd)) (NETEND 8080)) (cond
  ((not (HOELDINGWAE-HOELDER? HOELDERWAE)) (error "HOELDERWAE NOT A HOELDER"))
  (#t (run-server (lambda (HEAD BODEE)
    (FIEND HOELDERWAE (uri-path (request-uri HEAD)))
  ) 'http (list #:port NETEND)))
)))
